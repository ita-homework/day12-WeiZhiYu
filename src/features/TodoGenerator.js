import { useState } from "react";
import { nanoid } from 'nanoid'
import { useDispatch } from "react-redux";
import { addTodoItem } from "./todoItemSlice";

const TodoGenerator = () => {
    const dispatch = useDispatch()

    const [todoItemText, setTodoItemText] = useState("")
    const onChange = (event) => {
        setTodoItemText(event.target.value)
    }

    const onSubmit = (event) => {
        event.preventDefault()
        if (todoItemText.length !== 0) {
            const newTodoItem = {
                id: nanoid(),
                text: todoItemText,
                done: false,
            }
            dispatch(addTodoItem(newTodoItem))
            setTodoItemText("")
        }
    }
    return (
        <div>
            <form onSubmit={onSubmit}>
                <input type="text" id="newTodoItem" name="newTodoItem" value={todoItemText} onChange={onChange}/>
                <button type='submit'>add</button>
            </form>
        </div>
    )
}

export default TodoGenerator