import { useSelector } from "react-redux"
import TodoItem from "./TodoItem"

const TodoGroup = () => {
    const todoList = useSelector(state => state.todoItem.todoList)

    return (
        <div className="todo-group">
            {todoList.map((todoItem) => <TodoItem key={todoItem.id} todoItem={todoItem} />)}
        </div>
    )
}

export default TodoGroup