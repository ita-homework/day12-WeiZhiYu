# O

- Redux: Redux is a pattern and library for managing and updating "global" state. In yesterday, we learned react initially and wrote two demos, todoList and counter. In the code that was written at the time, it was cumbersome to pass parameters between layers. A global store is created in redux, making parameter modification and access easy.

# R

I am excited.

# I

Today, the teacher delivered a highly comprehensible lecture. I can understand what the teacher teaches and keep up with the pace of the class.

# D

I will consolidate what I learned today and try to learn more.