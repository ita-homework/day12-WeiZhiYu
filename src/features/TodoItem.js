import {useDispatch} from "react-redux"
import { changeDoneStatus, removeTodoItem } from "./todoItemSlice"

const TodoItem = (props) => {
    const { todoItem } = props 
    const dispatch = useDispatch()
    const switchDoneStatus = () => dispatch(changeDoneStatus(todoItem.id))
    const removeItem = () => dispatch(removeTodoItem(todoItem.id))

    return (
        <div className="todo-item">
            <label onClick={switchDoneStatus} className="todo-item-label">
                {todoItem.done ? <del>{todoItem.text}</del> : todoItem.text}
            </label>
            <button onClick={removeItem}>x</button>
        </div>
    )
}

export default TodoItem