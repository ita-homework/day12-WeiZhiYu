import { configureStore } from '@reduxjs/toolkit'
import todoItemReducer from '../features/todoItemSlice.js'

export default configureStore({
  reducer: {
    todoItem: todoItemReducer
  }
})