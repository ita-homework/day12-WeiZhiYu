import TodoGroup from "./TodoGroup"
import TodoGenerator from "./TodoGenerator"

const TodoList = () => {
    
    return (
        <div className="todo-list">Todo List
            <TodoGroup />
            <TodoGenerator />
        </div>
    )
}

export default TodoList