import { createSlice } from '@reduxjs/toolkit'

export const todoItemSlice = createSlice({
    name: 'todoItem',
    initialState: {
        todoList: [],
    },
    reducers: {
        addTodoItem: (state, action) => {
            state.todoList.push(action.payload)
        },
        changeDoneStatus: (state, action) => {
            state.todoList = state.todoList.map(todoItem => {
                if (todoItem.id === action.payload) {
                    todoItem.done = !todoItem.done
                }
                return todoItem
            })
        },
        removeTodoItem: (state, action) => {
            const index = state.todoList.findIndex(todoItem => todoItem.id === action.payload)
            state.todoList.splice(index, 1)
        }
    }
})

export default todoItemSlice.reducer
export const { addTodoItem, changeDoneStatus, removeTodoItem } = todoItemSlice.actions
